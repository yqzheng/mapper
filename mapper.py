"""adpated from kepler mapper"""
import numpy as np
import itertools
from collections import defaultdict
try:
    from collections.abc import Iterable
except:
    from collections import Iterable
from sklearn import cluster
from scipy.stats import iqr


def build_nerve(nodes):
    """build nerve from a list nodes - only up to 1-simplicies (edgges)
    :param nodes: dict, {node_id: list of sample_id}
    :return edges: edges of the nodes
    :return simplicies: complete list of simplicies
    """
    result = defaultdict(list)
    # create links between nodes that share samples
    candidates = itertools.combinations(nodes.keys(), 2)
    for candidate in candidates:
        if len(set(nodes[candidate[0]]).intersection(nodes[candidate[1]])) >= 1:
            result[candidate[0]].append(candidate[1])

    edges = [[v1, v2] for v1 in result for v2 in result[v1]]
    simplices = [[n] for n in nodes] + edges
    return result, simplices


def build_cover(lens, nr_cubes=10, overlap_perc=0.1):
    """
    :param lens: nd array, first column is index
    :param n_cubes: int number of intervals
    :param overlap_perc: float, overlaping percentage
    :return:
    """
    indexless_lens = lens[:, 1:]  # first column is index
    n_dims = indexless_lens.shape[1]  # dimension of lens
    # range of cover in each dimension
    bounds = (np.min(indexless_lens, axis=0), np.max(indexless_lens, axis=0)) # bounds in each dimension
    ranges = bounds[1] - bounds[0]

    if isinstance(nr_cubes, Iterable):
        nr_cubes = np.array(nr_cubes)  # customised nr_cubes can be different in each dimension
        assert (
            len(nr_cubes) == n_dims
        ), "custom cubes in each dimension must match number of dimensions"
    else:
        nr_cubes = np.repeat(nr_cubes, n_dims)  # or a single number

    if isinstance(overlap_perc, Iterable):
        overlap_perc = np.array(overlap_perc)  # can be different in each dimension
        assert (
            len(overlap_perc) == n_dims
        ), "custom overlap_perc in each dimension must match number of dimensions"
    else:
        overlap_perc = np.repeat(overlap_perc, n_dims)

    # inner range - used to define centers in each dimension
    inner_range = ((nr_cubes - 1) / nr_cubes) * ranges
    inset = (ranges - inner_range) / 2

    # radius of the bins - a little bit larger, okay..
    radius = ranges / (nr_cubes * (1 - overlap_perc)) / 2

    # centers in each dimension
    # zip_items = list(bounds)
    # zip_items.extend([nr_cubes, inset])
    zip_items = list(bounds) + [nr_cubes, inset]
    centers_per_dimension = [
        np.linspace(b + r, c - r, num=n) for b, c, n, r, in zip(*zip_items)
    ]

    # every combination of each dimension as centers
    centers = [np.array(c) for c in itertools.product(*centers_per_dimension)]

    # transform
    hypercubes = []
    for k, center in enumerate(centers):
        lowerbounds, upperbounds = center - radius, center + radius
        # slice the data that fall within the bounds
        slices = (indexless_lens >= lowerbounds) & (
            indexless_lens <= upperbounds
        )
        hypercube = lens[np.invert(np.any(slices == False, axis=1))]  # must include index
        print(
            "There are {0} points in cube {1}".format(hypercube.shape[0], k)
        )
        # hypercube = lens[np.all(slices == True, axis=1)]
        if len(hypercube):  # remove empty cubes
            hypercubes.append(hypercube)

    return hypercubes


def mapper(
        lens, data=None, nr_cubes=10, overlap_perc=0.1,
        clusterer=cluster.AgglomerativeClustering(n_clusters=3),
        precomputed=False, remove_duplicate_node=False,
        min_cluster_samples=1, distance_threshold=25

):
    nodes = defaultdict(list)
    graph = {}
    meta = defaultdict(list)

    if data is None:
        data = lens
    print(
        "Mapping data shaped %s using lens shaped %s\n"
        % (str(data.shape), str(lens.shape))
    )
    index = np.arange(lens.shape[0])
    lens = np.c_[index, lens]
    data = np.c_[index, data]

    # build covers
    print(
        "Building covers..."
    )
    hypercubes = build_cover(
        lens, nr_cubes=nr_cubes,
        overlap_perc=overlap_perc
    )
    print(
        "There are {} cubes that are not empty".format(len(hypercubes))
    )
    #clusterer_params = clusterer.get_params()
    for k, hypercube in enumerate(hypercubes):
        if hypercube.shape[0] >= min_cluster_samples:
            ids = [int(n) for n in hypercube[:, 0]]
            data_cube = data[ids]
            fit_data = data_cube[:, 1:]
            if precomputed:  # data is a distance matrix
                fit_data = fit_data[:, ids]
                ## single linkage
                if clusterer.__class__.__name__ == "AgglomerativeClustering":
                    distance_vec = fit_data[np.nonzero(np.triu(fit_data))]
                    if len(distance_vec) < 2: # only 1 points or 2 points
                        clusterer.set_params(n_clusters=1, distance_threshold=None)
                    else:
                        # get a histogram of the distance?
                        # TODO: use kde to find the local minium of distance distribution as threshold
                        # Freedman-Diaconis rule to choose bins
                        num_bins = np.ceil(
                            (distance_vec.max() - distance_vec.min()) /
                            (2 * iqr(distance_vec) * len(distance_vec) ** (-1/3))
                        )
                        hist = np.histogram(distance_vec, bins=int(num_bins))
                        print("distance histogram is", hist[0])
                        print("\n distance bins is", hist[1])
                        th = np.percentile(
                            distance_vec, q=distance_threshold
                        )
                        print("\n Distance threshold is {}".format(th))
                        clusterer.set_params(
                            n_clusters=None, distance_threshold=th
                        )
            cluster_predictions = clusterer.fit_predict(fit_data)  # do clustering
            print(
                "Found {} clusters.".format(
                    len(np.unique(
                        cluster_predictions[cluster_predictions > -1]
                    ))
                )
            )
            # record the clusters into nodes
            for idx, pred in np.c_[hypercube[:, 0], cluster_predictions]:
                if pred != -1 and not np.isnan(pred):
                    cluster_id = "cube{0}_cluster{1}".format(k, int(pred))
                    nodes[cluster_id].append(int(idx))
        else:
            print(
                "Cube {} is empty".format(int(k))
            )
    if remove_duplicate_node:
        nodes = _remove_duplicate_nodes(nodes)

        # build nerve
    edges, simplices = build_nerve(nodes)
    graph["nodes"] = nodes
    graph["links"] = edges
    graph["simplices"] = simplices
    graph["meta_data"] = {
        "nr_cubes": nr_cubes,
        "overlap_perc": overlap_perc,
        "lens": lens,
        "clusterer": str(clusterer)
    }
    graph["meta_nodes"] = meta

    print(
        "Done. Created {0} edges and {1} nodes."
            .format(sum(len(v) for k, v in edges.items()), len(nodes))
    )

    return graph


def _remove_duplicate_nodes(nodes):
    """
    remove duplicate nodes
    :param nodes: dict, `{node_id: ids of points}`
    :return:
    """
    dup_items = defaultdict(list)
    for node_id, items in nodes.items():
        dup_items[frozenset(items)].append(node_id)

    dup_nodes = {
        "|".join(node_id_list): list(frozen_items)
        for frozen_items, node_id_list in dup_items.items()
    }
    total_merges = len(nodes) - len(dup_nodes)
    print(
        "Merged {} duplicate nodes".format(total_merges)
    )
    print(
        "Number of nodes before merge: {0}; after merge {1}".format(len(nodes), len(dup_nodes))
    )
    return dup_nodes
