import os
import numpy as np
import kmapper as km
from scipy.spatial.distance import squareform, pdist
from scipy.io import loadmat
from sklearn import manifold
import argparse
from sklearn.cluster import AgglomerativeClustering
import pickle
from mapper import mapper
from visualisation import visualise_mapper


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--subject", type=int, dest="subject_id")
    parser.add_argument("--encoding", type=str, dest="encoding")
    parser.add_argument("--method", type=str, dest="method")
    parser.add_argument("--n_components", type=int, dest="n_components")
    parser.add_argument(
        "--overlap_perc", default=0.8, dest="overlap_perc",
        type=float, nargs='?', help="Overlap percentage of the covers"
    )
    parser.add_argument(
        "--nr_cubes", default=30, dest="nr_cubes", nargs='?',
        help="Number of covers", type=int
    )
    parser.add_argument(
        "--distance_threshold", type=float, dest="distance_threshold", default=25,
        help="distance threshold for single linkage clustering"
    )
    parser.add_argument(
        "--metric", default='euclidean', dest="metric", nargs='?',
        help="metric used for clustering", type=str
    )
    return parser.parse_args()


def read_subject_data(subject_id, encoding):
    fname = "{0}/tfMRI_WM_{1}_Atlas.dtseries.mat"\
        .format(subject_id, encoding)
    data = loadmat(fname)
    return data['data'].T


if __name__ == "__main__":
    args = parse_arguments()
    subject_id = args.subject_id
    encoding = args.encoding
    method = args.method
    n_components = args.n_components
    nr_cubes = args.nr_cubes
    overlap_perc = args.overlap_perc
    distance_threshold = args.distance_threshold
    metric = args.metric
    data = read_subject_data(subject_id, encoding)
    lens_fname = "{0}/tfMRI_WM_{1}_Atlas." \
                 "dtseries.{2}.n_components.{3}.npy"\
        .format(subject_id, encoding, method, n_components)
    dist_fname = "{0}/tfMRI_WM_{1}_Atlas." \
                 "dtseries.{2}.npy"\
        .format(subject_id, encoding, metric)
    if os.path.isfile(dist_fname):
        dist = np.load(dist_fname)
    else:
        dist = squareform(pdist(data, metric))
        np.save(dist_fname, dist)
    if os.path.isfile(lens_fname):
        lens = np.load(lens_fname)
    elif method == "TSNE":
        method_to_call = getattr(manifold, method)
        transformer = method_to_call(n_components=n_components, metric="precomputed")
        lens = transformer.fit_transform(dist)
    else:
        method_to_call = getattr(manifold, method)
        transformer = method_to_call(n_components=n_components)
        lens = transformer.fit_transform(data)
        np.save(lens_fname, lens)

    # load distance
    # km_mapper = km.KeplerMapper(verbose=1)
    clusterer = AgglomerativeClustering(linkage='single', affinity='precomputed',
                                        compute_full_tree=True)

    graph = mapper(
        lens=lens, data=dist, nr_cubes=nr_cubes, overlap_perc=overlap_perc,
        clusterer=clusterer, precomputed=True, distance_threshold=int(distance_threshold),
        min_cluster_samples=2, remove_duplicate_node=True
    )

    fname = "{0}/tfMRI_WM_{1}_Atlas." \
                 "dtseries.{2}.n_components.{3}.{4}.overlap_perc.{5}.nr_cubes.{6}.distance_threshold.{7}.pickle"\
        .format(subject_id, encoding, method, n_components, metric, overlap_perc, nr_cubes, distance_threshold)
    with open(fname, "wb") as f:
        pickle.dump(graph, f, protocol=pickle.HIGHEST_PROTOCOL)

    # visualise
    colors = np.zeros((data.shape[0], ))
    # open block onset
    color_fnames = ["0bk_body.txt", "2bk_body.txt", "0bk_tools.txt", "2bk_tools.txt",
                    "0bk_places.txt", "2bk_places.txt", "0bk_faces.txt", "2bk_faces.txt"]
    for color_fname in color_fnames:
        block = np.loadtxt(fname="{}/".format(subject_id) + color_fname)
        block = block.flatten()
        colors[int((block[0]+2.5)//0.72 +2) : int((block[0]+27.5) // 0.72+3)] = 1

    visualise_mapper(
        graph, color_function=colors, title=os.path.splitext(os.path.basename(fname))[0],
        path_html=os.path.splitext(fname)[0] + ".html", cmap="viridis", nbins=8
    )
    #km_mapper.visualize(graph, color_function=colors, title=os.path.splitext(os.path.basename(fname))[0],
    #                 path_html=os.path.splitext(fname)[0] + ".html")
