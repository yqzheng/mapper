syntax

```python hcp_wm_mapper.py --subject=100307 --encoding=LR --metric=cityblock --method=TSNE --overlap_perc=0.4 --nr_cubes=15 --n_components=2 --distance_threshold=60```
